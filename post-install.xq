xquery version "3.1";

import module namespace odd-serializer="http://bdn-edition.de/ns/odd-serializer";
import module namespace odd-test = "https://sade.textgrid.de/ns/odd-test" at "test.xq";
import module namespace test="http://exist-db.org/xquery/xqsuite" at "resource:org/exist/xquery/lib/xqsuite/xqsuite.xql";

declare variable $sysout := util:log-system-out("installation of ODD serializer done.");
declare variable $tests := test:suite(util:list-functions("https://sade.textgrid.de/ns/odd-test"));



util:log-system-out("&#10;" || serialize($tests) || "&#10;"),
file:serialize(<tests time="{current-dateTime()}">{ $tests }</tests>, system:get-exist-home() || util:system-property("file.separator") || "tests-odd-results.xml", ()),
odd-serializer:make-sample()
