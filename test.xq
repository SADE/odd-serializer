xquery version "3.1";

(: This is the main test script for the module content/main.xqm.
 : It is supposed to run during post installation stage.
 :
 : @version 1.0.0
 : @author Michelle Weidling
 : :)

module namespace odd-test = "https://sade.textgrid.de/ns/odd-test";

import module namespace odd-serializer="http://bdn-edition.de/ns/odd-serializer";

declare namespace test="http://exist-db.org/xquery/xqsuite";


(: Test to test the test :)
declare
    %test:name("FAIL")
    %test:args("<milestone/>")
    %test:assertFalse
    function odd-test:test-basic-000($node as element(*)) {
        false()
};

declare
    %test:name("Basic: teiHeader")
    %test:args("<teiHeader xmlns=""http://www.tei-c.org/ns/1.0""/>")
    %test:assertEmpty

    function odd-test:test-basic-001($node as element(*)) {
        odd-serializer:serialize($node)
};


declare
    %test:name("Basic: comments")
    %test:args("<body xmlns=""http://www.tei-c.org/ns/1.0""><!-- some comment --></body>")
    %test:assertEmpty

    function odd-test:test-basic-002($node as element(*)) {
        odd-serializer:serialize($node)
};

declare
    %test:name("Basic: Text")
    %test:args("<body xmlns=""http://www.tei-c.org/ns/1.0"">some text</body>")
    %test:assertEquals("some text")

    function odd-test:test-basic-003($node as element(*)) {
        odd-serializer:serialize($node)
};

declare
    %test:name("Basic: div")
    %test:args("<div type=""section"" xmlns=""http://www.tei-c.org/ns/1.0"">some text</div>")
    %test:assertEquals("<div class=""schema-section"" xmlns=""http://www.w3.org/1999/xhtml"">some text</div>")

    %test:args("<div xml:id=""section"" xmlns=""http://www.tei-c.org/ns/1.0"">some text</div>")
    %test:assertEquals("<div class=""schema-div"" xmlns=""http://www.w3.org/1999/xhtml"">some text</div>")

    %test:args("<div xmlns=""http://www.tei-c.org/ns/1.0"">some text</div>")
    %test:assertEquals("<div xmlns=""http://www.w3.org/1999/xhtml"">some text</div>")

    function odd-test:test-basic-004($node as element(*)) {
        odd-serializer:serialize($node)
};

declare
    %test:name("Basic: head")
    %test:args("<div xmlns=""http://www.tei-c.org/ns/1.0""><head>some text</head></div>")
    %test:assertXPath("$result//h2[@id]")

    %test:args("<div xmlns=""http://www.tei-c.org/ns/1.0""><div xmlns=""http://www.tei-c.org/ns/1.0""><head>some text</head></div></div>")
    %test:assertXPath("$result//h3[@id]")

    %test:args("<div xmlns=""http://www.tei-c.org/ns/1.0""><div xmlns=""http://www.tei-c.org/ns/1.0""><div xmlns=""http://www.tei-c.org/ns/1.0""><head>some text</head></div></div></div>")
    %test:assertXPath("$result//h4[@id]")

    %test:args("<div xmlns=""http://www.tei-c.org/ns/1.0""><div xmlns=""http://www.tei-c.org/ns/1.0""><div xmlns=""http://www.tei-c.org/ns/1.0""><div xmlns=""http://www.tei-c.org/ns/1.0""><head>some text</head></div></div></div></div>")
    %test:assertXPath("$result//h5[@id]")

    %test:args("<div xmlns=""http://www.tei-c.org/ns/1.0""><div xmlns=""http://www.tei-c.org/ns/1.0""><div xmlns=""http://www.tei-c.org/ns/1.0""><div xmlns=""http://www.tei-c.org/ns/1.0""><div xmlns=""http://www.tei-c.org/ns/1.0""><head>some text</head></div></div></div></div></div>")
    %test:assertXPath("$result//div[@id and @class=""low-level-heading""]")

    function odd-test:test-basic-005($node as element(*)) {
        odd-serializer:serialize($node)
};

declare
    %test:name("Basic: note")
    %test:args("<note xmlns=""http://www.tei-c.org/ns/1.0"">some text</note>")
    %test:assertXPath("$result[self::div[@class=""schema-info""]]")

    function odd-test:test-basic-006($node as element(*)) {
        odd-serializer:serialize($node)
};

declare
    %test:name("Basic: p")
    %test:args("<p xmlns=""http://www.tei-c.org/ns/1.0"">some text</p>")
    %test:assertXPath("$result[self::p]")

    function odd-test:test-basic-007($node as element(*)) {
        odd-serializer:serialize($node)
};

declare
    %test:name("Basic: list and li")
    %test:args("<list xmlns=""http://www.tei-c.org/ns/1.0""><item>some item</item></list>")
    %test:assertXPath("$result[self::ul]/child::li")

    function odd-test:test-basic-008($node as element(*)) {
        odd-serializer:serialize($node)
};

declare
    %test:name("Basic: specList and specDesc")
    %test:args("<specList xmlns=""http://www.tei-c.org/ns/1.0""><specDesc key=""note""/></specList>")
    %test:assertXPath("$result[self::ul]/child::li/a[@href =""http://www.tei-c.org/release/doc/tei-p5-doc/en/html/ref-note.html""]")

    function odd-test:test-basic-009($node as element(*)) {
        odd-serializer:serialize($node)
};

declare
    %test:name("Basic: eg")
    %test:args("<eg xmlns=""http://www.tei-c.org/ns/1.0""><tei/></eg>")
    %test:assertXPath("$result//text() = ""&gt;""")

    function odd-test:test-basic-010($node as element(*)) {
        odd-serializer:serialize($node)
};

declare
    %test:name("Basic: term")
    %test:args("<term xmlns=""http://www.tei-c.org/ns/1.0"">text</term>")
    %test:assertXPath("$result[self::em]")

    function odd-test:test-basic-011($node as element(*)) {
        odd-serializer:serialize($node)
};

declare
    %test:name("Basic: hi")
    %test:args("<hi xmlns=""http://www.tei-c.org/ns/1.0"">text</hi>")
    %test:assertXPath("$result[self::em]")

    function odd-test:test-basic-012($node as element(*)) {
        odd-serializer:serialize($node)
};

declare
    %test:name("Basic: lb")
    %test:args("<lb xmlns=""http://www.tei-c.org/ns/1.0""/>")
    %test:assertXPath("$result[self::br]")

    function odd-test:test-basic-013($node as element(*)) {
        odd-serializer:serialize($node)
};

declare
    %test:name("Basic: label")
    %test:args("<label xmlns=""http://www.tei-c.org/ns/1.0""/>")
    %test:assertXPath("$result[self::b]")

    function odd-test:test-basic-014($node as element(*)) {
        odd-serializer:serialize($node)
};

declare
    %test:name("Basic: tables")
    %test:args("<table xmlns=""http://www.tei-c.org/ns/1.0""><row><cell>text</cell></row></table>")
    %test:assertXPath("$result[self::table]/tr/td")

    function odd-test:test-basic-015($node as element(*)) {
        odd-serializer:serialize($node)
};

declare
    %test:name("Basic: back")
    %test:args("<back xmlns=""http://www.tei-c.org/ns/1.0""><row><cell>text</cell></row></back>")
    %test:assertEmpty

    function odd-test:test-basic-016($node as element(*)) {
        odd-serializer:serialize($node)
};

declare
    %test:name("Basic: gi")
    %test:args("<gi xmlns=""http://www.tei-c.org/ns/1.0"">note</gi>")
    %test:assertXPath("$result[self::a[@href=""http://www.tei-c.org/release/doc/tei-p5-doc/en/html/ref-note.html""]]")

    function odd-test:test-basic-017($node as element(*)) {
        odd-serializer:serialize($node)
};

declare
    %test:name("Basic: ref")
    %test:args("<ref xmlns=""http://www.tei-c.org/ns/1.0"" href=""somewhere""/>")
    %test:assertXPath("$result[self::a[@href=""somewhere""]]")

    function odd-test:test-basic-018($node as element(*)) {
        odd-serializer:serialize($node)
};


declare
    %test:name("TOC")
    %test:args("<div xmlns=""http://www.tei-c.org/ns/1.0""><head>some text</head></div>")
(:    %test:assertXPath("$result[self::ul[@class=""level-1""]/li/a[@class=""schema-link, schema-link-1""]]"):)
    %test:assertXPath("$result[self::ul[@class=""level-1""]]//a[contains(@class, ""schema-link-1"")]")

    function odd-test:test-basic-018($node as element(*)) {
        odd-serializer:serialize-toc($node)
};
