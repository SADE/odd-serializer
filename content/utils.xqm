xquery version "3.1";

(:~
 : An auxiliary module that simply displays the rendered sample ODD.
 :
 : @author Michelle Weidling
 : @since 1.0.0
 :
 :)

module namespace odd-utils="http://bdn-edition.de/ns/odd-utils";

import module namespace templates="http://exist-db.org/xquery/templates";

declare %templates:wrap function odd-utils:schema($node as node(), $model as map(*)) {
    doc("/db/apps/odd-serializer/sample/odd.html")/*
};

declare %templates:wrap function odd-utils:schema-toc($node as node(), $model as map(*)) {
  doc("/db/apps/odd-serializer/sample/odd-toc.html")/*
};
