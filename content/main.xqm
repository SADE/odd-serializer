(:~
 : This module provides a serialization of a given ODD file to two SADE compliant
 : HTML pages:
 :    1. the content of the ODD itself
 :    2. a table of contents
 : This division enables users e.g. to opt for a grid layout.
 :
 : For the rendering only the elements in the tei:body of the ODD are considered.
 : Formal declarations as well as the information in the tei:teiHeader are omitted.
 :
 : @author Michelle Weidling
 : @version 1.0.0
 :
 :)

xquery version "3.1";

module namespace odd-serializer="http://bdn-edition.de/ns/odd-serializer";

declare namespace eg="http://www.tei-c.org/ns/Examples";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

import module namespace code-view="http://bdn-edition.de/ns/code-view";

(:~
 : The main function. Creates an HTML file for the tei:body of the ODD as well
 : as an HTML file for the table of contents.
 :
 : @author Michelle Weidling
 : @param $path-to-odd Absolute path in the db where the ODD is stored
 : @param $coll-to-store Absolute path that indicates where the HTML pages should be saved to
 : @return The paths where the HTML files are stored
 :
 :)
declare function odd-serializer:main($path-to-odd as xs:string, $coll-to-store as xs:string)
as xs:string* {
  let $odd-file := doc($path-to-odd)
  let $html :=
    element xhtml:div {
      odd-serializer:serialize($odd-file)
    }
  let $toc :=
    element xhtml:div {
      attribute class {"schema-toc"},
      odd-serializer:serialize-toc($odd-file//tei:body)
    }
  return
    (xmldb:store($coll-to-store, "odd.html", $html),
    xmldb:store($coll-to-store, "odd-toc.html", $toc))
};


(:~
 : Transforms all descendants of tei:body to XHTML elements.
 :
 : @author Michelle Weidling
 : @param $odd-file The nodes of the ODD file
 : @return The respective XHTML elements
 :
 :)
declare function odd-serializer:serialize($odd-file as node()*) as node()* {
  for $node in $odd-file
  return
    typeswitch ($node)
    case text() return
      $node

    case comment() return
      ()

    case element(tei:teiHeader) return
      ()

    case element(tei:div) return
      element xhtml:div {
        if($node/@type = "section") then
          attribute class {"schema-section"}
        else if($node/@xml:id) then
          attribute class {"schema-div"}
        else
          (),
        odd-serializer:serialize($node/node())
      }    

    case element(tei:head) return
      let $level := string(count($node/ancestor::tei:div))
      let $id := generate-id($node)
      return
        switch ($level)
          case "1" return
            <div class="block-header">
              <h2 id="{$id}">
                <span class="title">
                  {odd-serializer:serialize($node/node())}
                </span>
                <span class="decoration"/>
                <span class="decoration"/>
                <span class="decoration"/>
              </h2>
            </div>
          case "2" return
            <h3 id="{$id}">{odd-serializer:serialize($node/node())}</h3>
          case "3" return
            <h4 id="{$id}">{odd-serializer:serialize($node/node())}</h4>
          case "4" return
            <h5 id="{$id}">{odd-serializer:serialize($node/node())}</h5>
          default return
            <div id="{$id}" class="low-level-heading">
              {odd-serializer:serialize($node/node())}
            </div>

    (: @@TODO     :)
    case element(tei:note) return
      <div class="schema-info">
        <i class="fas fa-info-circle"></i>
        {text{" "},
        odd-serializer:serialize($node/node())}
      </div>

    case element(tei:p) return
      local:copy-element($node)

    case element(tei:list) return
      <ul>{odd-serializer:serialize($node/node())}</ul>

    case element(tei:item) return
      <li>{odd-serializer:serialize($node/node())}</li>

    case element(tei:specList) return
      <ul>{odd-serializer:serialize($node/node())}</ul>

    case element(tei:specDesc) return
      let $url := "http://www.tei-c.org/release/doc/tei-p5-doc/en/html/ref-" || $node/@key || ".html"
      return
        <li>
          <a href="{$url}" target="_blank">
            {text{" "}, string($node/@key)}
          </a>
        </li>

    case element(eg:egXML) return
      element xhtml:div {
        attribute class {"example"},
        code-view:main($node)
      }

    (:@@@TODO :)
    case element(tei:eg) return
      element xhtml:div {
        attribute class {"example"},
        code-view:main($node)
      }

    case element(tei:term) return
      <em>{odd-serializer:serialize($node/node())}</em>

    case element(tei:hi) return
      <em>{odd-serializer:serialize($node/node())}</em>

    case element(tei:lb) return
      <br/>

    case element(tei:label) return
      <b>{odd-serializer:serialize($node/node())}</b>

    case element(tei:table) return
      <table>{odd-serializer:serialize($node/node())}</table>

    case element(tei:row) return
      <tr>{odd-serializer:serialize($node/node())}</tr>

    case element(tei:cell) return
      <td>{odd-serializer:serialize($node/node())}</td>

    case element(tei:back) return ()

    case element(tei:gi) return
      let $url := "http://www.tei-c.org/release/doc/tei-p5-doc/en/html/ref-" || $node/text() || ".html"
      return
        <a href="{$url}" target="_blank">
          {text{" "}, odd-serializer:serialize($node/node())}
        </a>

    case element(tei:ref) return
      <a href="{$node/@href}" target="_blank">
        {text{" "}, odd-serializer:serialize($node/node())}
      </a>

    default return
      odd-serializer:serialize($node/node())
};

(:~
 : Transforms all descendants of tei:body to XHTML elements.
 :
 : @author Michelle Weidling
 : @param $odd-file The nodes of the ODD file
 : @return The respective XHTML elements
 :
 :)
declare function odd-serializer:serialize-toc($odd-body as node()*) as node()* {
  for $node in $odd-body return
    typeswitch ($node)
    case text() return
      ()

    case element(tei:head) return
      let $id := generate-id($node)
      let $text := $node/string()
      let $level := count($node/ancestor::tei:div)
      let $class := "schema-link-" || $level
      return
        <li>
          <a class="{concat("schema-link ", $class)}" href="#{$id}">
            {$text}
          </a>
        </li>

    case element(tei:div) return
      let $level := count($node/ancestor::tei:div) + 1
      let $class := "level-" || $level
      return
        <ul class="{$class}">
          {odd-serializer:serialize-toc($node/node())}
        </ul>

    default return
      odd-serializer:serialize-toc($node/node())
};


(:~
 : An auxiliary function
 :
 : @author Michelle Weidling
 : @param $node Node to be copied
 : @return The copied node
 :
 :)
declare function local:copy-element($node as node()) as node() {
  element {name($node)} {
    odd-serializer:serialize($node/node())
  }
};


(:~
 : Creates the two HTML pages for a sample ODD stored in /sample/.
 :
 : @author Michelle Weidling
 : @return The paths to the stored HTML files
 :
 :)
declare function odd-serializer:make-sample() as xs:string* {
    let $doc := "/db/apps/odd-serializer/sample/example-odd.xml"
    let $coll := "/db/apps/odd-serializer/sample/"

    return odd-serializer:main($doc, $coll)
};
